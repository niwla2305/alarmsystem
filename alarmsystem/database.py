from peewee import *

db = SqliteDatabase('alarmsystem.db')


class Token(Model):
    uid = BigIntegerField()
    description = TextField()

    class Meta:
        database = db


class TgUser(Model):
    userid = BigIntegerField()
    trusted = BooleanField(default=True)
    admin = BooleanField(default=False)
    description = CharField()

    class Meta:
        database = db


db.create_tables([Token, TgUser])

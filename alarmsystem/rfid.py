from telegram import *
from telegram.ext import *
import rfid_helper
from util import *

def list_tokens(update, context):
    if is_admin(update.effective_user.id) is True:
        tokens = []
        for token in Token.select():
            tokens.append(
                str(token.id)
                + ": "
                + str(token.uid)
                + " => "
                + str(token.description)
            )
        print(tokens)
        update.message.reply_text("\n".join(str(e) for e in tokens))
    else:
        update.message.reply_text(
            "Du bist nicht berechtigt diesen Befehl zu nutzen."
        )

def delete_token(update, context):
    if is_admin(update.effective_user.id) is True:
        id = update.message.text[13:]
        update.message.reply_text(id)
        rfid_helper.delete_by_id(id)
        update.message.reply_text("Removed")

    else:
        update.message.reply_text(
            "Du bist nicht berechtigt diesen Befehl zu nutzen."
        )

def get_token_description(update, context):
    if is_trusted(update.effective_user.id) is True:
        time.sleep(2)
        update.message.reply_text(
            "Bitte halte den Token, von dem du die Beschreibung haben willst gegen den Leser."
        )
        while True:
            returned = rfid_helper.check_for_tokens()
            print("looking for tokens .....")
            if returned:
                print(returned)
                break
        row = rfid_helper.Token.get(rfid_helper.Token.uid == returned)
        update.message.reply_text(row.description)
        set_pickle("rfid_blocked.p", False)
    else:
        update.message.reply_text(
            "Du bist nicht berechtigt diesen Befehl zu nutzen."
        )

DESCRIPTION = range(4)
DELETE_ID = range(4)

uid = 000

# Who knows, maybe removing this would break everything?
rfid_blocked = False

def add_token(update, context):
    if is_admin(update.effective_user.id) is True:
        set_pickle("active.p", False)
        set_pickle("rfid_blocked.p", True)
        time.sleep(5)
        update.message.reply_text(
            "Okay, ein neuer Token. Bitte halte ihn vor das Lesegerät an der Tür"
        )
        while True:
            returned = rfid_helper.check_for_tokens()
            print("looking for tokens .....")
            if returned:
                print(returned)
                break
        global uid
        uid = returned
        print(rfid_helper.check_token_availability(uid))
        if rfid_helper.check_token_availability(uid) is False:
            update.message.reply_text(
                "Token wurde gefunden. Bitte gib eine Beschreibung ein."
            )
        else:
            update.message.reply_text(
                "Dieser Token existiert schon und wird jetzt gelöscht"
            )

            rfid_helper.add_or_remove_token(uid, "DELETED")
            set_pickle("rfid_blocked", False)
            return ConversationHandler.END
    else:
        update.message.reply_text(
            "Du bist nicht berechtigt diesen Befehl zu nutzen."
        )
    print("DEBUG: End of add_token function reached")
    return DESCRIPTION

def add_description(update, context):
    print("add_desc shoul have started")
    #rfid_helper.add_or_remove_token(uid, context.message.text)
    rfid_helper.add_or_remove_token(uid, update.message.text)
    print("Und hier passiert auch was")
    update.message.reply_text("Okay, ich füge das zur Datenbank hinzu.")
    global rfid_blocked
    set_pickle("rfid_blocked.p", False)
    return ConversationHandler.END

def cancel(update, context):
    update.message.reply_text("Abbruch.")
    return ConversationHandler.END

add_token_conversation = ConversationHandler(
    entry_points=[CommandHandler("token", add_token)],
    states={DESCRIPTION: [MessageHandler(Filters.text, add_description)]},
    fallbacks=[CommandHandler("cancel", cancel)],
)

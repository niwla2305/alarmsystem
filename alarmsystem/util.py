import json
import os
import pickle
import time

import telegram

import RPi.GPIO as GPIO
from database import *


def getconfig(setting):
    with open("config/config.json", 'r') as config_file:
        config_raw = config_file.read()
        configuration = json.loads(config_raw)
        return configuration[setting]


bot = telegram.Bot(getconfig("token"))


def is_trusted(userid):
    trusted_ids = []
    for user in TgUser.select():  # .where(TgUser.trusted is True):
        print(user.trusted)
        if user.trusted is True:
            trusted_ids.append(user.userid)
    if userid in trusted_ids:
        return True
    else:
        return False


def is_admin(userid):
    admin_ids = []
    for user in TgUser.select(): #.where(TgUser.admin is True):
        if user.admin is True:
            admin_ids.append(user.userid)
    if userid in admin_ids:
        return True
    else:
        return False

def add_user(userid, trusted, admin, description):
    TgUser.create(userid=userid, trusted=trusted, admin=admin, description=description)

def set_pickle(name, value):
    pickle.dump(value, open("pickles/" + name, "wb"))


def read_pickle(name):
    return pickle.load(open("pickles/" + name, "rb"))


def trigger_alarm():
    GPIO.output(getconfig('alarm_pin'), GPIO.HIGH)


def read_sensor():
    return GPIO.input(getconfig('sensor_pin'))


def check_door_and_trigger_alarm():
    #print("read sensor:" + str(read_sensor()))
    #print("active: "+str(read_pickle("active.p")))
    if read_sensor() == 1 and read_pickle("active.p") is True:
        # Door was opened
        if read_pickle("sound.p") is True:
            # Sound is activated, trigger siren
            trigger_alarm()
        os.system(getconfig("photo_command"))
        return True
    else:
        # Don't trigger alarm
        GPIO.output(getconfig('alarm_pin'), GPIO.LOW)
        time.sleep(1)
        return False


def send_message(message, reply_markup=None):
    for user in TgUser.select():
        if is_trusted(user.userid):
            bot.send_message(user.userid, message, reply_markup=reply_markup, parse_mode="Markdown")


def send_photo(path):
    for user in TgUser.select():  # .where(TgUser.trusted is True):
        if is_trusted(user.userid):
            bot.send_photo(user.userid, open(path, 'rb'))

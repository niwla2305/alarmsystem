import pickle
import time
from util import *
import mfrc522
import telegram

import RPi.GPIO as GPIO
from database import *

bot = telegram.Bot(getconfig("token"))

GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.OUT)
GPIO.setup(23, GPIO.OUT)

MIFAREReader = mfrc522.MFRC522()


def check_token_availability(uid):
    try:
        available = Token.get(Token.uid == uid)
    except:
        available = 0

    if not available == 0:
        return True
    else:
        return False


def check_token_availability_by_id(id):
    try:
        available = Token.get(Token.id == id)
    except:
        available = 0

    if not available == 0:
        return True
    else:
        return False


def add_or_remove_token(uid, description):
    if check_token_availability(uid) is False:
        Token.create(uid=uid, description=description)
    else:
        print("Token deleted")
        todelete = Token.get(Token.uid == uid)
        print(todelete)
        todelete.delete_instance()


def delete_by_id(id):
    if check_token_availability_by_id(id) is False:
        return False
    else:
        print("Token deleted")
        todelete = Token.get(Token.id == id)
        print(todelete)
        todelete.delete_instance()


def check_for_tokens():
    (status, TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)

    (status, uid) = MIFAREReader.MFRC522_Anticoll()

    if status == MIFAREReader.MI_OK:
        uid = int(str(uid[0]) + str(uid[1]) + str(uid[2]) + str(uid[3]))
        time.sleep(2)
        return uid


def red_light():
    GPIO.output(18, GPIO.HIGH)
    time.sleep(2)
    GPIO.output(18, GPIO.LOW)


def green_light():
    GPIO.output(23, GPIO.HIGH)
    time.sleep(2)
    GPIO.output(23, GPIO.LOW)


def green_blink():
    for i in range(10):
        GPIO.output(23, GPIO.HIGH)
        time.sleep(0.1)
        GPIO.output(23, GPIO.LOW)
        time.sleep(0.1)


def red_blink():
    for i in range(10):
        GPIO.output(18, GPIO.HIGH)
        time.sleep(0.1)
        GPIO.output(18, GPIO.LOW)
        time.sleep(0.1)


def rfid_check():
    set_pickle("rfid_blocked.p", False)
    print("Looking for tokens")
    try:
        if read_pickle("rfid_blocked.p") is False:
            MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)
            (status, uid) = MIFAREReader.MFRC522_Anticoll()

            # If we have the UID, continue
            if status == MIFAREReader.MI_OK:
                # Print UID
                uid = int(str(uid[0]) + str(uid[1]) + str(uid[2]) + str(uid[3]))
                print(uid)
                valid_tokens = []
                for token in Token.select():
                    valid_tokens.append(token.uid)
                if uid in valid_tokens:
                    query = Token.select().where(Token.uid == uid)
                    description = "empty"
                    for i in query:
                        description = i.description
                    green_light()

                    if read_pickle("active.p") is True:
                        set_pickle("active.p", False)
                        print(
                            "Pickle active=" + str(read_pickle("active.p"))
                        )
                        send_message(
                            "Alarmanlage wurde per RFID deaktiviert von: "
                            + description,
                        )
                        print("Alarm toggled off")
                        red_blink()
                    else:
                        set_pickle("active.p", True)
                        print(
                            "Pickle active=" + str(read_pickle("active.p"))
                        )
                        print("Alarm toggled on")
                        print(
                            send_message(
                                "Alarmanlage wurde per RFID aktiviert von: "
                                + description,
                            )
                        )
                        green_blink()
                else:
                    red_light()

                time.sleep(2)
        else:
            return

    except KeyboardInterrupt:
        GPIO.cleanup()

from telegram import *
from telegram.ext import *

from util import *

if getconfig("rfid_enabled") is True:
    from rfid import *

print(getconfig("token"))
updater = Updater(getconfig("token"), use_context=True)

GPIO.setmode(GPIO.BCM)

GPIO.setup(getconfig("sensor_pin"), GPIO.IN)
GPIO.setup(getconfig("alarm_pin"), GPIO.OUT)


# helper functions


def make_username_string(update):
    if update.effective_user.username:
        return " (@" + str(update.effective_user.username + str(")"))
    else:
        return ""


def start(update, context):
    reply_markup2 = ReplyKeyboardMarkup(
        [["/disable_alarm", "/activate_alarm"], ["/disable_sound", "/enable_sound"]],
        one_time_keyboard=False,
        resize_keyboard=True,
    )
    if is_trusted(update.effective_user.id) is True:
        send_message(
            "Der Alarmbot wurde gestartet von: "
            + str(update.effective_user.first_name)
            + make_username_string(update),
            reply_markup=reply_markup2,
        )
    else:
        update.message.reply_sticker(
            "CAACAgIAAxkBAAIDYF4sSjX_4uH8Xw8Ci2SBk3oOfZ4NAAKFAwACxKtoC-5zkyofbOifGAQ"
        )


def disable_sound(update, context):
    if is_trusted(update.effective_user.id) is True:
        set_pickle("sound.p", False)
        update.message.reply_sticker(
            getconfig("sticker_alarm_mute")
        )
        send_message(
            "Leiser Alarm eingestellt von: "
            + str(update.effective_user.first_name)
            + make_username_string(update)
        )
    else:
        update.message.reply_sticker(
            "CAACAgIAAxkBAAIDYF4sSjX_4uH8Xw8Ci2SBk3oOfZ4NAAKFAwACxKtoC-5zkyofbOifGAQ"
        )


def enable_sound(update, context):
    if is_trusted(update.effective_user.id) is True:
        set_pickle("sound.p", True)
        update.message.reply_sticker(
            getconfig("sticker_alarm_unmute")
        )
        send_message(
            "Lauter Alarm eingestellt von: "
            + str(update.effective_user.first_name)
            + make_username_string(update)
        )
    else:
        update.message.reply_sticker(
            "CAACAgIAAxkBAAIDYF4sSjX_4uH8Xw8Ci2SBk3oOfZ4NAAKFAwACxKtoC-5zkyofbOifGAQ"
        )


def disable_alarm(update, context):
    if is_trusted(update.effective_user.id) is True:
        update.message.reply_sticker(
            getconfig("sticker_alarm_off")
        )
        set_pickle("active.p", False)
        send_message(
            "Überwachung deaktiviert von: "
            + str(update.effective_user.first_name)
            + make_username_string(update)
        )
    else:
        update.message.reply_sticker(
            "CAACAgIAAxkBAAIDYF4sSjX_4uH8Xw8Ci2SBk3oOfZ4NAAKFAwACxKtoC-5zkyofbOifGAQ"
        )


def activate_alarm(update, context):
    print("start of scahrf")
    if is_trusted(update.effective_user.id) is True:
        update.message.reply_sticker(
            getconfig("sticker_alarm_on")
        )
        set_pickle("active.p", True)
        send_message(
            "Überwachung aktiviert von: "
            + str(update.effective_user.first_name)
            + make_username_string(update)
        )

    else:
        update.message.reply_sticker(
            "CAACAgIAAxkBAAIDYF4sSjX_4uH8Xw8Ci2SBk3oOfZ4NAAKFAwACxKtoC-5zkyofbOifGAQ"
        )


# Add User

USERID, DESCRIPTION_tg, TRUSTED, ADMIN = range(4)

userid_val = 0
description_val = str
trusted_val = admin_val = False

new_user_data = {}


def start_add_user(update, context):
    update.message.reply_text(
        "Neuen Benutzer registrieren: "
        "Gib bitte zuerst die userid an\n\n"
        "Du kannst sie mit @getidsbot ermitteln."
    )
    return USERID


def userid(update, context):
    # userid_val = int(update.message.text)
    new_user_data["userid"] = int(update.message.text)
    update.message.reply_text("okay, sende mir jetzt eine Beschreibung")
    return DESCRIPTION_tg


def description(update, context):
    print(update.message.text)
    # description_val = update.message.text
    new_user_data["description"] = update.message.text
    reply_keyboard = [["True", "False"]]
    update.message.reply_text(
        "Dem Nutzer soll vertraut werden:",
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
    )
    return TRUSTED


def trusted(update, context):
    new_user_data["trusted"] = update.message.text.lower() in [
        "true",
        "ja",
        "jup",
        "yes",
    ]
    # trusted_val = update.message.text.lower() in ["true", "ja", "jup", "yes"]
    reply_keyboard = [["True", "False"]]
    update.message.reply_text(
        "Der Nutzer soll Admin sein:",
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
    )
    return ADMIN


def admin(update, context):
    # admin_val = update.message.text.lower() in ["true", "ja", "jup", "yes"]
    new_user_data["admin"] = update.message.text.lower() in ["true", "ja", "jup", "yes"]
    global description_val
    print(new_user_data)
    add_user(
        new_user_data["userid"],
        new_user_data["trusted"],
        new_user_data["admin"],
        new_user_data["description"],
    )
    update.message.reply_sticker(
        "CAACAgIAAxkBAAIDXl4sSd29hQSah8jCvoBQXRT_9xzQAAKKAwACxKtoC4wPU7ueLzlYGAQ"
    )

    return ConversationHandler.END


def add_user_cancel(update, context):
    update.message.reply_text("Abbruch.")
    return ConversationHandler.END


conv_handler_add_user = ConversationHandler(
    entry_points=[CommandHandler("add_user", start_add_user)],
    states={
        USERID: [MessageHandler(Filters.text, userid)],
        DESCRIPTION_tg: [MessageHandler(Filters.text, description)],
        TRUSTED: [MessageHandler(Filters.text, trusted)],
        ADMIN: [MessageHandler(Filters.text, admin)],
    },
    fallbacks=[CommandHandler("cancel", add_user_cancel)],
)


def list_tg_users(update, context):
    print("kacken")
    if is_admin(update.effective_user.id) is True:
        print("jaja mach was de willst")
        users = []
        for user in TgUser.select():
            print("yay einuser")
            users.append(
                str(user.id)
                + ": "
                + str(user.userid)
                + " | Trusted: "
                + str(user.trusted)
                + " | Admin: "
                + str(user.admin)
                + " | "
                + str(user.description)
            )
        print(users)
        update.message.reply_text("\n".join(str(e) for e in users))
    else:
        update.message.reply_sticker(
            "CAACAgIAAxkBAAIDYF4sSjX_4uH8Xw8Ci2SBk3oOfZ4NAAKFAwACxKtoC-5zkyofbOifGAQ"
        )


def delete_tg_user(update, context):
    if is_admin(update.effective_user.id) is True:
        id = int(update.message.text[13:])
        update.message.reply_text(id)
        todelete = TgUser.get(TgUser.id == id)
        print(todelete)
        print(todelete.delete_instance())
        update.message.reply_text("Entfernt")
        update.message.reply_sticker(
            "CAACAgIAAxkBAAIDWl4sSMmu8wF2yB5oQ31iTUeIllhfAAKNAAMw1J0RVuNXh_pHjKcYBA"
        )

    else:
        update.message.reply_sticker(
            "CAACAgIAAxkBAAIDYF4sSjX_4uH8Xw8Ci2SBk3oOfZ4NAAKFAwACxKtoC-5zkyofbOifGAQ"
        )


def untrust_tg_user(update, context):
    if is_admin(update.effective_user.id) is True:
        id = int(update.message.text[13:])
        update.message.reply_text(id)
        tountrust = TgUser.get(TgUser.id == id)
        tountrust.trusted = False
        tountrust.save()
        update.message.reply_text("Untrusted.")
        update.message.reply_sticker(
            "CAACAgIAAxkBAAIDZF4scgJatgfWIYEixCf_elkjS3NWAAKLAwACxKtoC6h7YHBi7Wx1GAQ"
        )

    else:
        update.message.reply_sticker(
            "CAACAgIAAxkBAAIDYF4sSjX_4uH8Xw8Ci2SBk3oOfZ4NAAKFAwACxKtoC-5zkyofbOifGAQ"
        )


def trust_tg_user(update, context):
    if is_admin(update.effective_user.id) is True:
        id = int(update.message.text[11:])
        update.message.reply_text(id)
        totrust = TgUser.get(TgUser.id == id)
        totrust.trusted = True
        totrust.save()
        update.message.reply_text("Trusted.")
        update.message.reply_sticker(
            "CAACAgIAAxkBAAIDZF4scgJatgfWIYEixCf_elkjS3NWAAKLAwACxKtoC6h7YHBi7Wx1GAQ"
        )

    else:
        update.message.reply_sticker(
            "CAACAgIAAxkBAAIDYF4sSjX_4uH8Xw8Ci2SBk3oOfZ4NAAKFAwACxKtoC-5zkyofbOifGAQ"
        )


def unadminify_tg_user(update, context):
    # print(is_admin(update.effective_user.id))
    if is_admin(update.effective_user.id) is True:
        id = int(update.message.text[13:])
        print("dsdf")
        if TgUser.get(TgUser.id == id).userid == update.message.from_user.id:
            print("Well it should work")
            update.message.reply_sticker(
                "CAACAgIAAxkBAAIDYl4sbYPBwYDSlFE3BmqZx1gCVAABcgACfgMAAsSraAvhj55WHhBKdBgE"
            )
            return
        update.message.reply_text(id)
        tountrust = TgUser.get(TgUser.id == id)
        tountrust.admin = False
        tountrust.save()
        update.message.reply_text("Superuser Rechte entzogen..")
        update.message.reply_sticker(
            "CAACAgIAAxkBAAIDZF4scgJatgfWIYEixCf_elkjS3NWAAKLAwACxKtoC6h7YHBi7Wx1GAQ"
        )

    else:
        update.message.reply_sticker(
            "CAACAgIAAxkBAAIDYF4sSjX_4uH8Xw8Ci2SBk3oOfZ4NAAKFAwACxKtoC-5zkyofbOifGAQ"
        )


def adminify_tg_user(update, context):
    if is_admin(update.effective_user.id) is True:
        id = int(update.message.text[11:])
        update.message.reply_text(id)
        totrust = TgUser.get(TgUser.id == id)
        totrust.admin = True
        totrust.save()
        update.message.reply_text("Superuser Rechte gewährt.")
        update.message.reply_sticker(
            "CAACAgIAAxkBAAIDZF4scgJatgfWIYEixCf_elkjS3NWAAKLAwACxKtoC6h7YHBi7Wx1GAQ"
        )

    else:
        update.message.reply_sticker(
            "CAACAgIAAxkBAAIDYF4sSjX_4uH8Xw8Ci2SBk3oOfZ4NAAKFAwACxKtoC-5zkyofbOifGAQ"
        )


# RFID releated stuff
if getconfig("rfid_enabled") is True:
    # RFID Commands
    updater.dispatcher.add_handler(CommandHandler("list_tokens", list_tokens))
    updater.dispatcher.add_handler(
        CommandHandler("get_token_description", get_token_description)
    )
    updater.dispatcher.add_handler(CommandHandler("delete_token", delete_token))
    updater.dispatcher.add_handler(add_token_conversation)


# register non RFID commands
updater.dispatcher.add_handler(CommandHandler("start", start))
updater.dispatcher.add_handler(CommandHandler("disable_alarm", disable_alarm))
updater.dispatcher.add_handler(CommandHandler("activate_alarm", activate_alarm))
updater.dispatcher.add_handler(CommandHandler("disable_sound", disable_sound))
updater.dispatcher.add_handler(CommandHandler("enable_sound", enable_sound))
updater.dispatcher.add_handler(CommandHandler("list_users", list_tg_users))
updater.dispatcher.add_handler(CommandHandler("delete_user", delete_tg_user))
updater.dispatcher.add_handler(CommandHandler("untrust_user", untrust_tg_user))
updater.dispatcher.add_handler(CommandHandler("trust_user", trust_tg_user))
updater.dispatcher.add_handler(CommandHandler("unadmin_user", unadminify_tg_user))
updater.dispatcher.add_handler(CommandHandler("admin_user", adminify_tg_user))

updater.dispatcher.add_handler(conv_handler_add_user)

updater.start_polling()


while True:
    if getconfig("rfid_enabled") is True:
        rfid_helper.rfid_check()
    if check_door_and_trigger_alarm() is True:
        send_message(
            "Alarm! Die Tür wurde geöffnet. In wenigen Sekunden werden sie ein Bild erhalten."
        )
        time.sleep(6)
        send_photo("image.png")
